Jaguza
======

Jaguza is a FREE WordPress theme available in the wordpress.org Theme repository. 
It is a fully responsive, translation-ready theme built to make your blog very fast and look amazing. 
Jaguza is a child theme of the awesome TwentyTwelve theme developed by the WordPress Team. 
<p>Features include:
<ul>
<li>Unlimited theme color options with over 10 pre-built</li>
<li>Heavily-customizable homepage</li>
<li>Amazing easily-customizable slider(that can be disabled)</li>
<li>A stylish social bar</li>
<li>In-built theme help</li>
<li>Front page template with its own widgets </li>
<li>An optional no-sidebar page template</li>
</ul>
All these and many more are easy to change from a classy, super powerful, easy-to-use Admin panel built on
the premium Slightly Modified Options Framework(SMOF). 
Jaguza was built for speed, beauty and ease-of-use</p>

Demo: <a href="http://demos.kanzucode.com/?themedemo=jaguza&hidebar" target="_blank">http://demos.kanzucode.com/?themedemo=jaguza&hidebar</a>
Download theme: <a href="http://wordpress.org/themes/jaguza/" target="_blank">http://wordpress.org/themes/jaguza/</a>

