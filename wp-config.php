<?php
/**
 * The base configurations of the WordPress.
 *
 * This file has the following configurations: MySQL settings, Table Prefix,
 * Secret Keys, and ABSPATH. You can find more information by visiting
 * {@link https://codex.wordpress.org/Editing_wp-config.php Editing wp-config.php}
 * Codex page. You can get the MySQL settings from your web host.
 *
 * This file is used by the wp-config.php creation script during the
 * installation. You don't have to use the web site, you can just copy this file
 * to "wp-config.php" and fill in the values.
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'wpsandbox');

/** MySQL database username */
define('DB_USER', 'homestead');

/** MySQL database password */
define('DB_PASSWORD', 'secret');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'P<`|-(>4R4hu/4Lm6HZN-QH%Ji/PK b|s-%iDH+0|k7.SVv1|M|p1or]g;P{Suek');
define('SECURE_AUTH_KEY',  'KL@Kqe2I~rK6!QLNl)Ndajv4E]k5L?d~;yZp%c/v)|1+nGvuH#-}*Ww16)Y(n4`5');
define('LOGGED_IN_KEY',    'lm-amZ]%?itg(qWil([fbGVlHS+x.p?Y}6XHfC3S>,Rsp]-5qY8Fne~yE{ve=iu0');
define('NONCE_KEY',        'TE,}z-a~>pQ]%lJwpX59= HZ,NU(lM)ZWi_b7-kB2--ru)}(_aB):)phlqA]1cD4');
define('AUTH_SALT',        'f0I=Mw,E@$C*ZolpJ 1L<R2UYT:nM,am[en .Jc+#&&5y+D*sVe[^FwdXX^PP$<k');
define('SECURE_AUTH_SALT', 'rX[&.}0SNx)4][j,HL?GX2qB@]cEoy5w%lB8|i]e;Gj;(0x>>F7{O2<tpUzvXWvg');
define('LOGGED_IN_SALT',   '$g4<L6A:vAfSTO58`zWPhki5d!i?Lq<ofjfJ yqw;y2rvd5Q&A:VSKsw7HFTp@3(');
define('NONCE_SALT',       'e6eN3]JZSp3u$wkO%|+mz^*L96mPagR}KIaLv3u_qKSegH{ISfdkBydMTM*,?4Ey');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each a unique
 * prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
